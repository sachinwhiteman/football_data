
from sqlalchemy import Integer, ForeignKey, String, Column, DateTime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

Base = declarative_base()


class Country(Base):
    __tablename__ = 'countries'

    id = Column(Integer, primary_key=True)
    name = Column(String(25), unique=True)

    def __repr__(self):
        return '<Country (name={})>'.format(self.name)


class Club(Base):
    __tablename__ = 'clubs'

    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True)

    def __repr__(self):
        return '<Club (name={})>'.format(self.name)


class League(Base):
    __tablename__ = 'leagues'

    id = Column(Integer, primary_key=True)
    name = Column(String(25), unique=True)
    country_id = Column(Integer, ForeignKey('countries.id'))

    country = relationship('Country', backpopulates='leagues')

    def __repr__(self):
        return '<League (name={})>'.format(self.name)

Country.leagues = relationship('League', backpopulates='country')


class Season(Base):
    __tablename__ = 'seasons'

    id = Column(Integer, primary_key=True)
    name = Column(String(25), unique=True)
    league_id = Column(Integer, ForeignKey('leagues.id'))

    League = relationship('League', backpopulates='seasons')

    def __repr__(self):
        return '<Season (name={})>'.format(self.name)

League.seasons = relationship('Season', backpopulates='league')


class Match(Base):
    __tablename__ = 'matches'

    id = Column(Integer, primary_key=True)
    date = Column(DateTime)
    season_id = Column(Integer, ForeignKey('seasons.id'))
    home_team_id = Column(Integer, ForeignKey('clubs.id'))
    away_team_id = Column(Integer, ForeignKey('clubs.id'))
    home_goals = Column(Integer)
    away_goals = Column(Integer)

    season = relationship('Season', backpopulates='matches')
    home_team = relationship('Club', backpopulates='home_matches')
    away_team = relationship('Club', backpopulates='away_matches')

    def __repr__(self):
        return '<Match ({} vs {}, Date:{})>'.format(
            self.home_team, self.away_team, str(self.date))

Season.matches = relationship('Match', backpopulates='season')
Club.home_matches = relationship('Match', backpopulates='home_team')
Club.away_matches = relationship('Match', backpopulates='away_team')

