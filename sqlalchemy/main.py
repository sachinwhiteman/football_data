import json
import csv
import requests

from io import StringIO

from sqlalchemy.exc import IntegrityError
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from .db import Base, League, Match, Season, Country, Club

engine = create_engine('postgresql://postgres:orange@localhost/tplr')
Session = sessionmaker(bind=engine)

Base.metadata.create_all(engine)


def add_country(name):

    session = Session()
    try:
        country = Country(name=name)
        session.add(country)
    except IntegrityError:
        country = session.query(Country).filter_by(name=name).first()
    session.commit()

    return country.id


def add_club(name):

    session = Session()
    try:
        club = Club(name=name)
        session.add(club)
    except IntegrityError:
        club = session.query(Club).filter_by(name=name).first()
    session.commit()

    return club.id


def add_league(name, country_id):
    session = Session()
    try:
        league = League(name=name, country_id=country_id)
        session.add(league)
    except IntegrityError:
        league = session.query(League).filter_by(name=name, country_id=country_id).first()
    session.commit()

    return league.id


def add_season(name, league_id):
    session = Session()
    try:
        season = Season(name=name, league_id=league_id)
        session.add(season)
    except IntegrityError:
        season = session.query(Season).filter_by(name=name).first()
    session.commit()

    return season.id


def add_match(date, home_team, away_team, home_team_goals, away_team_goals):

    session = Session()
    try:
        match = Match(
            date=date,
            home_team_id=home_team,
            away_team_id=away_team,
            home_team_goals=home_team_goals,
            away_team_goals=away_team_goals
        )
        session.add(match)
    except IntegrityError:
        match = session.query(Match).filter_by(date=date,
                                               home_team_id=home_team,
                                               away_team_id=away_team
                                               ).first()
    session.commit()

    return match.id


def main():
    """ main function"""

    with open("football_data.json", "r") as data_file:
        football_data = json.load(data_file)
        for data in football_data[::50]:
            country = add_country(data['country'])
            league = add_league(name=data['league'], country_id=country)
            season = add_season(name=data['season'], league_id=league)
            r = requests.get("http://{}".format(data["result"]))
            buffer = StringIO(r.text)
            reader = csv.DictReader(buffer)
            for row in reader:
                home_team = add_club(row['HomeTeam'])
                away_team = add_club(row['AwayTeam'])
                home_team_goals = row['FTHG']
                away_team_goals = row['FTAG']
                date = row['Date']
                match = add_match(date=date,
                                  season_id=season,
                                  home_team=home_team,
                                  away_team=away_team,
                                  home_team_goals=home_team_goals,
                                  away_team_goals=away_team_goals
                                  )
                print(match)
