from sqlalchemy import Column, Integer, String, DateTime, Text, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

Base = declarative_base()


class Country(Base):

    __tablename__ = 'Countries'

    id = Column(Integer, primary_key=True)
    name = Column(String(20))

    def __repr__(self):
        return "<Country (name={})>".format(self.name)


class Season(Base):
    __tablename__ = 'seasons'

    id = Column(Integer, primary_key=True)
    name = Column(String)

    def __repr__(self):
        return "<Season (name={})>".format(self.name)


class Club(Base):

    __tablename__ = 'clubs'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    country_id = Column(Integer, ForeignKey('Country.id'))

    country = relationship('Country', backpopulates='clubs')

    def __repr__(self):
        return "<Club (name={})>".format(self.name)


class League(Base):

    __tablename__ = 'leagues'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    country_id = Column(Integer, ForeignKey('countries.id'))
    season_id = Column(Integer, ForeignKey('seasons.id'))

    country = relationship('Country', backpopulates='leagues')

    def __repr__(self):
        return "<League (name={})>".format(self.name)


class Match(Base):
    __tablename__ = 'matches'

    id = Column(Integer, primary_key=True)
    date = Column(DateTime)
    season_id = Column(Integer, ForeignKey('season.id'))
    home_team_id = Column(Integer, ForeignKey('clubs.id'))
    away_team_id = Column(Integer, ForeignKey('clubs.id'))
    home_goals = Column(Integer)
    away_goals = Column(Integer)

    home_team = relationship('Club', backpopulates=' ')
    away_team = relationship('Club', backpopulates=' ')

    def __repr__(self):
        return "<Match ({} vs {})>".format(self.home_team_id, self.away_team_id)
