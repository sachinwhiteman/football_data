
import scrapy
from bs4 import BeautifulSoup
from ..items import FootballDataItem


class LeagueSpider(scrapy.Spider):
    name = 'leagues'
    start_urls = ['http://www.football-data.co.uk/data.php', ]

    def parse(self, response):
        urls = response.xpath('//a[contains(text(), "Results")]/@href').extract()
        for url in urls:
            yield response.follow(url, callback=self.parse_seasons)

    def parse_seasons(self, response):
        soup = BeautifulSoup(response.body, 'html.parser')
        country = response.url[:-5].split('/')[3]
        seasons = soup.find_all(["i", "a"])
        seasons = list(filter(
            lambda season: 'Season' in str(season) or ".csv" in str(season),
            seasons))
        i, length, result_list = 0, len(seasons), []
        while i < length:
            season = seasons[i].text.split()[1]
            i = i + 1
            try:
                while seasons[i].has_attr('href'):
                    result_list.append(
                        {'country': country,
                         'season': season,
                         'league': seasons[i].text,
                         'result': "http://www.football-data.co.uk/{}".format(seasons[i]['href'])
                         }
                    )
                    i = i + 1
            except IndexError:
                break
        results = map(lambda result: FootballDataItem(
            country=result['country'],
            league=result['league'],
            season=result['season'],
            result=result['result']
        ), result_list)
        for result in results:
            yield result
